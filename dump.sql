--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: ruffguide; Type: SCHEMA; Schema: -; Owner: ruffdbadmin
--

CREATE SCHEMA ruffguide;


ALTER SCHEMA ruffguide OWNER TO ruffdbadmin;

SET search_path = ruffguide, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO ruffdbadmin;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO ruffdbadmin;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO ruffdbadmin;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO ruffdbadmin;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO ruffdbadmin;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO ruffdbadmin;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO ruffdbadmin;

--
-- Name: auth_user_groups; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO ruffdbadmin;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO ruffdbadmin;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO ruffdbadmin;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO ruffdbadmin;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO ruffdbadmin;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO ruffdbadmin;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO ruffdbadmin;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO ruffdbadmin;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO ruffdbadmin;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO ruffdbadmin;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO ruffdbadmin;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO ruffdbadmin;

--
-- Name: ruff_events; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE ruff_events (
    id integer NOT NULL,
    type character varying(50) NOT NULL,
    description text NOT NULL,
    organiser character varying(100) NOT NULL,
    cost text NOT NULL,
    contact character varying(200) NOT NULL,
    location character varying(200) NOT NULL,
    website character varying(200) NOT NULL,
    end_date character varying(15) NOT NULL,
    start_date character varying(15) NOT NULL,
    end_time character varying(10) NOT NULL,
    start_time character varying(10) NOT NULL,
    name character varying(50) NOT NULL,
    photo character varying(200)[] NOT NULL,
    date timestamp with time zone NOT NULL
);


ALTER TABLE ruff_events OWNER TO ruffdbadmin;

--
-- Name: ruff_events_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE ruff_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ruff_events_id_seq OWNER TO ruffdbadmin;

--
-- Name: ruff_events_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE ruff_events_id_seq OWNED BY ruff_events.id;


--
-- Name: ruff_hikereviews; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE ruff_hikereviews (
    id integer NOT NULL,
    "user" character varying(200) NOT NULL,
    comments text NOT NULL,
    rating double precision NOT NULL,
    hike_id integer NOT NULL,
    date timestamp with time zone NOT NULL,
    photo character varying(200)[] NOT NULL
);


ALTER TABLE ruff_hikereviews OWNER TO ruffdbadmin;

--
-- Name: ruff_hikereviews_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE ruff_hikereviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ruff_hikereviews_id_seq OWNER TO ruffdbadmin;

--
-- Name: ruff_hikereviews_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE ruff_hikereviews_id_seq OWNED BY ruff_hikereviews.id;


--
-- Name: ruff_hikes; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE ruff_hikes (
    id integer NOT NULL,
    location text NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    difficulty character varying(100) NOT NULL,
    distance double precision NOT NULL,
    coordinates text NOT NULL,
    more_info_link character varying(300) NOT NULL,
    leash boolean NOT NULL,
    days_of_week text NOT NULL,
    photo character varying(200)[] NOT NULL,
    date timestamp with time zone NOT NULL
);


ALTER TABLE ruff_hikes OWNER TO ruffdbadmin;

--
-- Name: ruff_hikes_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE ruff_hikes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ruff_hikes_id_seq OWNER TO ruffdbadmin;

--
-- Name: ruff_hikes_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE ruff_hikes_id_seq OWNED BY ruff_hikes.id;


--
-- Name: ruff_parkreviews; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE ruff_parkreviews (
    id integer NOT NULL,
    "user" character varying(200) NOT NULL,
    comments text NOT NULL,
    rating double precision NOT NULL,
    park_id integer NOT NULL,
    date timestamp with time zone NOT NULL,
    photo character varying(200)[] NOT NULL
);


ALTER TABLE ruff_parkreviews OWNER TO ruffdbadmin;

--
-- Name: ruff_parkreviews_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE ruff_parkreviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ruff_parkreviews_id_seq OWNER TO ruffdbadmin;

--
-- Name: ruff_parkreviews_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE ruff_parkreviews_id_seq OWNED BY ruff_parkreviews.id;


--
-- Name: ruff_parks; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE ruff_parks (
    id integer NOT NULL,
    location text NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    leash boolean NOT NULL,
    days_of_week text NOT NULL,
    photo character varying(200)[] NOT NULL,
    date timestamp with time zone NOT NULL
);


ALTER TABLE ruff_parks OWNER TO ruffdbadmin;

--
-- Name: ruff_parks_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE ruff_parks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ruff_parks_id_seq OWNER TO ruffdbadmin;

--
-- Name: ruff_parks_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE ruff_parks_id_seq OWNED BY ruff_parks.id;


--
-- Name: ruff_restaurantreviews; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE ruff_restaurantreviews (
    id integer NOT NULL,
    "user" character varying(200) NOT NULL,
    comments text NOT NULL,
    rating double precision NOT NULL,
    restaurant_id integer NOT NULL,
    date timestamp with time zone NOT NULL,
    photo character varying(200)[] NOT NULL
);


ALTER TABLE ruff_restaurantreviews OWNER TO ruffdbadmin;

--
-- Name: ruff_restaurantreviews_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE ruff_restaurantreviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ruff_restaurantreviews_id_seq OWNER TO ruffdbadmin;

--
-- Name: ruff_restaurantreviews_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE ruff_restaurantreviews_id_seq OWNED BY ruff_restaurantreviews.id;


--
-- Name: ruff_restaurants; Type: TABLE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE TABLE ruff_restaurants (
    id integer NOT NULL,
    type character varying(50) NOT NULL,
    name text NOT NULL,
    description text NOT NULL,
    phone_number character varying(20) NOT NULL,
    building_name character varying(50) NOT NULL,
    postcode character varying(10) NOT NULL,
    state character varying(50) NOT NULL,
    street_name character varying(20) NOT NULL,
    street_number integer NOT NULL,
    suburb character varying(50) NOT NULL,
    days_of_week text NOT NULL,
    photo character varying(200)[] NOT NULL,
    date timestamp with time zone NOT NULL
);


ALTER TABLE ruff_restaurants OWNER TO ruffdbadmin;

--
-- Name: ruff_restaurants_id_seq; Type: SEQUENCE; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE SEQUENCE ruff_restaurants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ruff_restaurants_id_seq OWNER TO ruffdbadmin;

--
-- Name: ruff_restaurants_id_seq; Type: SEQUENCE OWNED BY; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER SEQUENCE ruff_restaurants_id_seq OWNED BY ruff_restaurants.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: ruff_events id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_events ALTER COLUMN id SET DEFAULT nextval('ruff_events_id_seq'::regclass);


--
-- Name: ruff_hikereviews id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_hikereviews ALTER COLUMN id SET DEFAULT nextval('ruff_hikereviews_id_seq'::regclass);


--
-- Name: ruff_hikes id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_hikes ALTER COLUMN id SET DEFAULT nextval('ruff_hikes_id_seq'::regclass);


--
-- Name: ruff_parkreviews id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_parkreviews ALTER COLUMN id SET DEFAULT nextval('ruff_parkreviews_id_seq'::regclass);


--
-- Name: ruff_parks id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_parks ALTER COLUMN id SET DEFAULT nextval('ruff_parks_id_seq'::regclass);


--
-- Name: ruff_restaurantreviews id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_restaurantreviews ALTER COLUMN id SET DEFAULT nextval('ruff_restaurantreviews_id_seq'::regclass);


--
-- Name: ruff_restaurants id; Type: DEFAULT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_restaurants ALTER COLUMN id SET DEFAULT nextval('ruff_restaurants_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--



--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--



--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--

INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (1, 'Can add events', 1, 'add_events');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (2, 'Can change events', 1, 'change_events');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (3, 'Can delete events', 1, 'delete_events');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (4, 'Can add hike leash', 2, 'add_hikeleash');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (5, 'Can change hike leash', 2, 'change_hikeleash');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (6, 'Can delete hike leash', 2, 'delete_hikeleash');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (7, 'Can add hike reviews', 3, 'add_hikereviews');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (8, 'Can change hike reviews', 3, 'change_hikereviews');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (9, 'Can delete hike reviews', 3, 'delete_hikereviews');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (10, 'Can add hikes', 4, 'add_hikes');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (11, 'Can change hikes', 4, 'change_hikes');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (12, 'Can delete hikes', 4, 'delete_hikes');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (13, 'Can add holiday reviews', 5, 'add_holidayreviews');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (14, 'Can change holiday reviews', 5, 'change_holidayreviews');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (15, 'Can delete holiday reviews', 5, 'delete_holidayreviews');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (16, 'Can add holidays', 6, 'add_holidays');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (17, 'Can change holidays', 6, 'change_holidays');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (18, 'Can delete holidays', 6, 'delete_holidays');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (19, 'Can add park leash', 7, 'add_parkleash');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (20, 'Can change park leash', 7, 'change_parkleash');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (21, 'Can delete park leash', 7, 'delete_parkleash');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (22, 'Can add park reviews', 8, 'add_parkreviews');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (23, 'Can change park reviews', 8, 'change_parkreviews');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (24, 'Can delete park reviews', 8, 'delete_parkreviews');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (25, 'Can add parks', 9, 'add_parks');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (26, 'Can change parks', 9, 'change_parks');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (27, 'Can delete parks', 9, 'delete_parks');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (28, 'Can add restaurant reviews', 10, 'add_restaurantreviews');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (29, 'Can change restaurant reviews', 10, 'change_restaurantreviews');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (30, 'Can delete restaurant reviews', 10, 'delete_restaurantreviews');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (31, 'Can add restaurants', 11, 'add_restaurants');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (32, 'Can change restaurants', 11, 'change_restaurants');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (33, 'Can delete restaurants', 11, 'delete_restaurants');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (34, 'Can add log entry', 12, 'add_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (35, 'Can change log entry', 12, 'change_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (36, 'Can delete log entry', 12, 'delete_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (37, 'Can add permission', 13, 'add_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (38, 'Can change permission', 13, 'change_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (39, 'Can delete permission', 13, 'delete_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (40, 'Can add group', 14, 'add_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (41, 'Can change group', 14, 'change_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (42, 'Can delete group', 14, 'delete_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (43, 'Can add user', 15, 'add_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (44, 'Can change user', 15, 'change_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (45, 'Can delete user', 15, 'delete_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (46, 'Can add content type', 16, 'add_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (47, 'Can change content type', 16, 'change_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (48, 'Can delete content type', 16, 'delete_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (49, 'Can add session', 17, 'add_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (50, 'Can change session', 17, 'change_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (51, 'Can delete session', 17, 'delete_session');


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--

INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (1, 'pbkdf2_sha256$36000$6XYvF1SHqQfk$AeVEkasax+jI9npepzCSxIl0Mef60McooEWPZi4BMjQ=', '2017-11-24 13:07:44.189957+00', true, 'admin', '', '', 'digitalpawventures@hotmail.com', true, true, '2017-10-28 22:26:00.203892+00');


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--



--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--



--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--



--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--

INSERT INTO django_content_type (id, app_label, model) VALUES (1, 'ruff', 'events');
INSERT INTO django_content_type (id, app_label, model) VALUES (2, 'ruff', 'hikeleash');
INSERT INTO django_content_type (id, app_label, model) VALUES (3, 'ruff', 'hikereviews');
INSERT INTO django_content_type (id, app_label, model) VALUES (4, 'ruff', 'hikes');
INSERT INTO django_content_type (id, app_label, model) VALUES (5, 'ruff', 'holidayreviews');
INSERT INTO django_content_type (id, app_label, model) VALUES (6, 'ruff', 'holidays');
INSERT INTO django_content_type (id, app_label, model) VALUES (7, 'ruff', 'parkleash');
INSERT INTO django_content_type (id, app_label, model) VALUES (8, 'ruff', 'parkreviews');
INSERT INTO django_content_type (id, app_label, model) VALUES (9, 'ruff', 'parks');
INSERT INTO django_content_type (id, app_label, model) VALUES (10, 'ruff', 'restaurantreviews');
INSERT INTO django_content_type (id, app_label, model) VALUES (11, 'ruff', 'restaurants');
INSERT INTO django_content_type (id, app_label, model) VALUES (12, 'admin', 'logentry');
INSERT INTO django_content_type (id, app_label, model) VALUES (13, 'auth', 'permission');
INSERT INTO django_content_type (id, app_label, model) VALUES (14, 'auth', 'group');
INSERT INTO django_content_type (id, app_label, model) VALUES (15, 'auth', 'user');
INSERT INTO django_content_type (id, app_label, model) VALUES (16, 'contenttypes', 'contenttype');
INSERT INTO django_content_type (id, app_label, model) VALUES (17, 'sessions', 'session');


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--

INSERT INTO django_migrations (id, app, name, applied) VALUES (1, 'contenttypes', '0001_initial', '2017-10-25 10:54:12.52988+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (2, 'auth', '0001_initial', '2017-10-25 10:54:15.328474+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (3, 'admin', '0001_initial', '2017-10-25 10:54:16.255828+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (4, 'admin', '0002_logentry_remove_auto_add', '2017-10-25 10:54:17.501868+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (5, 'contenttypes', '0002_remove_content_type_name', '2017-10-25 10:54:18.132439+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (6, 'auth', '0002_alter_permission_name_max_length', '2017-10-25 10:54:18.680464+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (7, 'auth', '0003_alter_user_email_max_length', '2017-10-25 10:54:19.238436+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (8, 'auth', '0004_alter_user_username_opts', '2017-10-25 10:54:19.556343+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (9, 'auth', '0005_alter_user_last_login_null', '2017-10-25 10:54:20.096836+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (10, 'auth', '0006_require_contenttypes_0002', '2017-10-25 10:54:20.413851+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (11, 'auth', '0007_alter_validators_add_error_messages', '2017-10-25 10:54:20.733986+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (12, 'auth', '0008_alter_user_username_max_length', '2017-10-25 10:54:21.307645+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (13, 'ruff', '0001_initial', '2017-10-25 10:54:24.441962+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (14, 'ruff', '0002_auto_20171025_1054', '2017-10-25 10:54:25.189108+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (15, 'sessions', '0001_initial', '2017-10-25 10:54:25.926782+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (16, 'ruff', '0003_auto_20171025_2240', '2017-10-25 22:40:30.750107+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (17, 'ruff', '0004_remove_parkleash_times', '2017-10-25 23:00:29.664602+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (18, 'ruff', '0005_auto_20171025_2314', '2017-10-25 23:14:51.770301+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (19, 'ruff', '0006_auto_20171027_1957', '2017-10-27 19:57:52.025215+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (20, 'ruff', '0007_auto_20171029_0045', '2017-10-29 00:45:28.847232+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (21, 'ruff', '0008_auto_20171030_0756', '2017-10-30 07:56:59.907541+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (22, 'ruff', '0009_auto_20171031_0309', '2017-10-31 03:09:52.725536+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (23, 'ruff', '0010_auto_20171101_0153', '2017-11-01 01:54:08.177829+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (24, 'ruff', '0002_events_name', '2017-11-02 02:39:24.121007+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (25, 'ruff', '0003_auto_20171102_0240', '2017-11-02 02:40:30.413784+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (26, 'ruff', '0004_auto_20171102_0240', '2017-11-02 02:40:47.368279+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (27, 'ruff', '0005_auto_20171104_0027', '2017-11-04 00:27:24.135487+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (28, 'ruff', '0006_auto_20171104_0043', '2017-11-04 00:43:51.637561+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (29, 'ruff', '0007_auto_20171104_0127', '2017-11-04 01:28:08.423282+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (30, 'ruff', '0008_auto_20171104_0241', '2017-11-04 02:41:24.063924+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (31, 'ruff', '0009_auto_20171104_0253', '2017-11-04 02:53:12.936119+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (32, 'ruff', '0010_auto_20171104_0258', '2017-11-04 02:58:23.064464+00');
INSERT INTO django_migrations (id, app, name, applied) VALUES (33, 'ruff', '0011_auto_20171114_0503', '2017-11-14 05:03:52.072515+00');


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--

INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('rdphwkqqlnpxl7f6kmwq8imr0oj0dw6q', 'MzY4NzNmYWU2ODg4MTM4ZjM1MjUwYWNmMzgzNTllMDUzYjNlNWI4NTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlMjQxZmU0NjYyOTVmYTBkMzc3YmY3OTgzMDFjZDBjNDU2ODg2ZmRkIiwidXNlcl9sb2NhdGlvbiI6IntcImxhdGl0dWRlXCI6IFwiLTMzLjg5MjI1MTA5OTk5OTk5NlwiLCBcImxvbmdpdHVkZVwiOiBcIjE1MS4yNzE0NTM1XCJ9In0=', '2017-12-13 12:40:09.249114+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('hwchxtv7epcmp259q9o66lp2ezuxbx5f', 'OTA3YThkYjFkNGFjMjNmZDQxMWU3NTgwYTBhNmZkMmE1Nzk0MjdjMDp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ4MjA5OTk5OTk5NlwiLCBcImxvbmdpdHVkZVwiOiBcIi02My42NTg0ODAzOTk5OTk5OTVcIn0ifQ==', '2017-12-13 23:54:04.588206+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('orvbfrdrnkb5m8d5w7zelpfu7z2ix26y', 'YjgwN2Q1YjdhMTY1MThkYzg1NmY2MGI4MmJmYTE2ZGM5YjhkMDNiZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlMjQxZmU0NjYyOTVmYTBkMzc3YmY3OTgzMDFjZDBjNDU2ODg2ZmRkIn0=', '2017-11-13 12:35:11.967211+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('mlpyuehka7sgfofec5mizqja5mxxblzg', 'YjgwN2Q1YjdhMTY1MThkYzg1NmY2MGI4MmJmYTE2ZGM5YjhkMDNiZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlMjQxZmU0NjYyOTVmYTBkMzc3YmY3OTgzMDFjZDBjNDU2ODg2ZmRkIn0=', '2017-11-18 07:29:39.4019+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('4oq4ybjnxqju27u23iqdqs196uj86v8v', 'YjgwN2Q1YjdhMTY1MThkYzg1NmY2MGI4MmJmYTE2ZGM5YjhkMDNiZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlMjQxZmU0NjYyOTVmYTBkMzc3YmY3OTgzMDFjZDBjNDU2ODg2ZmRkIn0=', '2017-11-18 19:41:13.930199+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('sw6qmm2m7cpqtnq0c3alsrdylaqppndj', 'MDdiNWM0N2I3NWMxMjRiOGVmNjFiOTE0MmZlZjEyYzdlNjBhZDJjYjp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ4ODNcIiwgXCJsb25naXR1ZGVcIjogXCItNjMuNjU4NTQxNjAwMDAwMDFcIn0ifQ==', '2017-12-07 12:35:31.11103+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('bdxtcqmeyimweum4z4c1o04kjxz4jorv', 'YjgwN2Q1YjdhMTY1MThkYzg1NmY2MGI4MmJmYTE2ZGM5YjhkMDNiZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlMjQxZmU0NjYyOTVmYTBkMzc3YmY3OTgzMDFjZDBjNDU2ODg2ZmRkIn0=', '2017-11-19 01:27:10.144025+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('nvde9y1f4n0up99ym8104f8gsz04jupi', 'YjgwN2Q1YjdhMTY1MThkYzg1NmY2MGI4MmJmYTE2ZGM5YjhkMDNiZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlMjQxZmU0NjYyOTVmYTBkMzc3YmY3OTgzMDFjZDBjNDU2ODg2ZmRkIn0=', '2017-11-19 01:31:29.2273+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('ywetq4pya5omf4lokg3ksf6pomw0irim', 'YjgwN2Q1YjdhMTY1MThkYzg1NmY2MGI4MmJmYTE2ZGM5YjhkMDNiZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlMjQxZmU0NjYyOTVmYTBkMzc3YmY3OTgzMDFjZDBjNDU2ODg2ZmRkIn0=', '2017-11-22 02:10:34.852307+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('rgruk1g4eblz1124wlpnxw6ekd83rg5g', 'YjMwZGVkOGQzZDAzZmU4NzJmMmFjYjBkOTY5NGU0NmE0OGI5ZWZjMzp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ4ODY5OTk5OTk5NVwiLCBcImxvbmdpdHVkZVwiOiBcIi02My42NTg1Mzc4XCJ9In0=', '2017-12-08 03:30:36.878743+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('kiv50w7as595hv81r3lpdnzvdfhnenxh', 'YjgwN2Q1YjdhMTY1MThkYzg1NmY2MGI4MmJmYTE2ZGM5YjhkMDNiZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlMjQxZmU0NjYyOTVmYTBkMzc3YmY3OTgzMDFjZDBjNDU2ODg2ZmRkIn0=', '2017-12-06 21:27:08.034012+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('trvz42pua6ve50136r07tu6l7wngq6fo', 'NTE3NDdiMDQ5YjZkZTcwZDFjNmIzZTQ0NDYyZmYyMzAwNGJhODk0ZTp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIi0zMy44OTIyNTEwOTk5OTk5OTZcIiwgXCJsb25naXR1ZGVcIjogXCIxNTEuMjcxNDUzNVwifSJ9', '2017-12-11 12:42:29.279029+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('1mcfu64o5flbyimm84bbcu188qqojltu', 'YjgwN2Q1YjdhMTY1MThkYzg1NmY2MGI4MmJmYTE2ZGM5YjhkMDNiZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlMjQxZmU0NjYyOTVmYTBkMzc3YmY3OTgzMDFjZDBjNDU2ODg2ZmRkIn0=', '2017-12-08 13:07:44.310184+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('rdpxn4u5i7xfmniz67wr8thkp0h7p9uy', 'OTFiNjcxZGNlMjIwNjAxOTZkYWVkMjcwNDc3ZTE2YWYxZGU2MWQzYjp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIi0zMy44MzczOTQ3XCIsIFwibG9uZ2l0dWRlXCI6IFwiMTUxLjIwMDg2MDVcIn0ifQ==', '2017-12-08 13:29:32.349261+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('n04z3tvzqwajlpsqixnswmyzsf22wf3y', 'M2FiYzBiMjA3M2M5ZjM0NjNhMTg2YjVjZDE0ZmEwZjFjOTk1Y2FkZTp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ0NzJcIiwgXCJsb25naXR1ZGVcIjogXCItNjMuNjU4NTU1NVwifSJ9', '2017-12-11 23:20:41.217707+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('z2oufhi2vly419tcy9qki0silpwf01g2', 'NmRiOTZkMzZkNDgxNDQyZWMxYzYzMjhkOThhZjBlMWVjYjFhODgzMTp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ4MjhcIiwgXCJsb25naXR1ZGVcIjogXCItNjMuNjU4NTM4NjAwMDAwMDFcIn0ifQ==', '2017-12-11 23:24:24.422402+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('e65c29g02s5y6mbnjt961v7whnp8iqi8', 'M2JmMDFjZTIwYTQ2M2M5YzU3NGQ2YTU2N2E5YjM2N2I2YWIyM2YyNDp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ2NjRcIiwgXCJsb25naXR1ZGVcIjogXCItNjMuNjU4NTM1MlwifSJ9', '2017-12-11 23:24:43.701445+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('ozoypk992ssyoxlfafnyl01g7mu2mo4l', 'NWJkM2JlNTY3ZGM3MDdkMzE3NTgwNWI0OWEwNmE1ODA4NTM0MjUyNjp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ3NTdcIiwgXCJsb25naXR1ZGVcIjogXCItNjMuNjU4NTY5N1wifSJ9', '2017-12-11 23:35:17.193587+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('w9plyz8vohyd7c1rgvurhwqpf38en9r6', 'ZTFjNGFhMzIxMzIzYTIwZjEyMTU3ODVjZTMzMjdkZGMxNWUxOGQ2ODp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDUxMTVcIiwgXCJsb25naXR1ZGVcIjogXCItNjMuNjU4NTE1MVwifSJ9', '2017-12-07 11:59:50.509962+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('2aqbm9d7kfrek1xv3sihvj4x4cwjkhdu', 'NjBlOTU3MzU3NzRhOTE0NjRjNzFhNmZjNDJkZGE0N2EyZTUzZWNlNDp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ2MjU5OTk5OTk5NVwiLCBcImxvbmdpdHVkZVwiOiBcIi02My42NTg1MjU4XCJ9In0=', '2017-12-07 12:01:50.288944+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('5w91hwuflni29eu5r51xmvb8gjvw2a8k', 'Yjg5MDY1YWMxZDFiZjI5Yjk1NjI3NzZiM2FkZDBhZTA3OTk4YWIyZTp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ5MjM5OTk5OTk5NVwiLCBcImxvbmdpdHVkZVwiOiBcIi02My42NTg1Mzk2XCJ9In0=', '2017-12-07 12:03:08.900789+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('r9fwo1zwpi1d0necx8fomhqxs0sfqndx', 'OTU1YjNmNDUxNjY5MmNjYTczNzg5MDBkZjA1MTZjODAzZTNkZWM1ZDp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ3NDA5OTk5OTk5NVwiLCBcImxvbmdpdHVkZVwiOiBcIi02My42NTg1NDQ3XCJ9In0=', '2017-12-07 12:09:30.442412+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('5g2lpt83nflzzpwhc0i60wtj2zxafmfw', 'YmFlZTdkM2JiZGY2YmNjNjgzNzg4YmIzYTk5ZTk1YTM5MDMwNDBkNTp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ2NTFcIiwgXCJsb25naXR1ZGVcIjogXCItNjMuNjU4NTY0Mzk5OTk5OTk2XCJ9In0=', '2017-12-07 12:11:47.014814+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('pc6vqadicymvk4wvffpnzhwedw5a5qdr', 'Yzk5YzM2OGU1NjY0NjIzODVhNmNjYmM4ZmMxNmYxOTg3ODc2NzQzNTp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ2MjFcIiwgXCJsb25naXR1ZGVcIjogXCItNjMuNjU4NTMxOTk5OTk5OTk0XCJ9In0=', '2017-12-07 12:12:13.03171+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('5qdky41s15o1iieys5i6wi3il9vrc7wb', 'NWY5ZGUxYjMxMzEyMzViZDBjNTY2ZTM0NmJlNGRiZDUwMTU0OGY3Njp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ3MDQ5OTk5OTk5NVwiLCBcImxvbmdpdHVkZVwiOiBcIi02My42NTg1NDg3MDAwMDAwMDRcIn0ifQ==', '2017-12-07 12:14:08.636387+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('qqcl5sz6wi1udyx2jhvsqti9c55imb1w', 'Y2I4MGRjYWQzZTYyZmUyYjRhZjk2ZDg3YjNiMmFiNjgyOWEzNGQzYzp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ2MjU5OTk5OTk5NVwiLCBcImxvbmdpdHVkZVwiOiBcIi02My42NTg1NDc4OTk5OTk5OTVcIn0ifQ==', '2017-12-07 12:15:06.540531+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('u4r67qik32u2g5ht0k61j4ncbo9cwpsi', 'MDJmNGI4MzNjZTVmMzQ3YzdkMmM5NzRkNGQxMTZmZjM4NjU4M2RiMzp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ5MTc5OTk5OTk5NlwiLCBcImxvbmdpdHVkZVwiOiBcIi02My42NTg1MzI1OTk5OTk5OTRcIn0ifQ==', '2017-12-11 23:36:31.127999+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('7azvq442dyh3hb4ql34ookggrh5up4i8', 'OTk0OWU4ZGQyZDk4MjdhZTUwYWZhMDdlZTM1OTU1Y2U1YmFlYTliYzp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ5OTlcIiwgXCJsb25naXR1ZGVcIjogXCItNjMuNjU4NTI5NTk5OTk5OTk0XCJ9In0=', '2017-12-11 23:37:08.197912+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('tmaog6fh2okj8z0be7p7uexpafy8y8oa', 'ZTY1ZDVjN2E4MDdhZGJjMTJmN2JlYjNjMGU1NjNkMTJlYmRhZWIzZTp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIi0zMy44NjUxNDNcIiwgXCJsb25naXR1ZGVcIjogXCIxNTEuMjA5OVwifSJ9', '2017-12-11 23:44:27.398859+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('1tsxdc8t4rchtqon3x2cd8ppdold7a77', 'MDU5NjA1MTRjMjU4OTUwMWJhMDczMzE5MjFkZTY4MTEwMjIwZWJhZTp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIi0zNy44MTRcIiwgXCJsb25naXR1ZGVcIjogXCIxNDQuOTYzMzJcIn0ifQ==', '2017-12-11 23:48:35.838965+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('s4axmkgb91xc1dorhk8wmkz8jqadyrqq', 'MDU5NjA1MTRjMjU4OTUwMWJhMDczMzE5MjFkZTY4MTEwMjIwZWJhZTp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIi0zNy44MTRcIiwgXCJsb25naXR1ZGVcIjogXCIxNDQuOTYzMzJcIn0ifQ==', '2017-12-11 23:50:43.510528+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('1pbylffk7uanma5viumi4cc2h13ur472', 'MDU5NjA1MTRjMjU4OTUwMWJhMDczMzE5MjFkZTY4MTEwMjIwZWJhZTp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIi0zNy44MTRcIiwgXCJsb25naXR1ZGVcIjogXCIxNDQuOTYzMzJcIn0ifQ==', '2017-12-11 23:51:25.862297+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('wvwprwxqz0k6axjd4dbgil3vflrkan07', 'MDU5NjA1MTRjMjU4OTUwMWJhMDczMzE5MjFkZTY4MTEwMjIwZWJhZTp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIi0zNy44MTRcIiwgXCJsb25naXR1ZGVcIjogXCIxNDQuOTYzMzJcIn0ifQ==', '2017-12-12 00:19:38.033148+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('ll9e5apqdcft943tvjl2lxus376h609n', 'MDU5NjA1MTRjMjU4OTUwMWJhMDczMzE5MjFkZTY4MTEwMjIwZWJhZTp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIi0zNy44MTRcIiwgXCJsb25naXR1ZGVcIjogXCIxNDQuOTYzMzJcIn0ifQ==', '2017-12-13 02:44:22.961082+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('ffv4hc6p962np9e16nfnribzxe5vu48l', 'ZTFmZWY1ZWE5ODUyYTRlMzJlNjhhODVjYzQ0YWE5OWZjZGZjYzVkMzp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQzNTg5OTk5OTk5NlwiLCBcImxvbmdpdHVkZVwiOiBcIi02My42NTg1OTk5OTk5OTk5OVwifSJ9', '2017-12-12 10:54:54.717468+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('lcwlt6hokaoel5vsjxunst7oa7xm1kfa', 'Y2ZjMjQxZDY1MzQzMzA1MWUyNGNiNTNhYjQ2NGJmNDcwYzljNmI1ODp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ3NzNcIiwgXCJsb25naXR1ZGVcIjogXCItNjMuNjU4NTk4Mjk5OTk5OTk0XCJ9In0=', '2017-12-12 10:55:36.497047+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('7ag8ei3285et0fgborxxztl6i5phsnoq', 'ZGRiODU0YzkxMDUxYmRiZGY2NDI4ZTYwMzVhNzdmNGFjOTM3ZGVlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlMjQxZmU0NjYyOTVmYTBkMzc3YmY3OTgzMDFjZDBjNDU2ODg2ZmRkIiwidXNlcl9sb2NhdGlvbiI6IntcImxhdGl0dWRlXCI6IFwiNDQuNjYwNDg2ODk5OTk5OTk1XCIsIFwibG9uZ2l0dWRlXCI6IFwiLTYzLjY1ODUzNzhcIn0ifQ==', '2017-12-13 23:51:11.222111+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('3yd1qck770to76dk6fzg6zfa6q090sgl', 'MDU5NjA1MTRjMjU4OTUwMWJhMDczMzE5MjFkZTY4MTEwMjIwZWJhZTp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIi0zNy44MTRcIiwgXCJsb25naXR1ZGVcIjogXCIxNDQuOTYzMzJcIn0ifQ==', '2017-12-13 02:20:06.095462+00');
INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('if0u0rxqcqe1xhulxd7a57dtd5ktsmz3', 'ODNkNzI3ODY3MzkyMDdlNGU0Yzk3YzY2OTllNGYxMmU1NjJlNmU0ZTp7InVzZXJfbG9jYXRpb24iOiJ7XCJsYXRpdHVkZVwiOiBcIjQ0LjY2MDQ4NjVcIiwgXCJsb25naXR1ZGVcIjogXCItNjMuNjU4NTMwMTk5OTk5OTk0XCJ9In0=', '2017-12-13 02:21:06.596823+00');


--
-- Data for Name: ruff_events; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--

INSERT INTO ruff_events (id, type, description, organiser, cost, contact, location, website, end_date, start_date, end_time, start_time, name, photo, date) VALUES (6, 'Festival', 'Melbourne’s only dog music festival is back and ready to rock its third year. As always, the event is a perfect opportunity to bask in the sun with your pooch and enjoy music from local artists.  The lineup brings together classics and new talent including Abbie Cardwell, Deborah Conway, DJ Mac Fleetwood, Evangeline and Lanks. Listening to so much music, it can be easy to work up an appetite. Melbourne’s favourite food and drink vendors will be on hand to help you out including Moon Dog Brewery, Rice & Dice, Let’s Waffle & Shake and Jerry’s Veggie Burgers. And of course, the festival wouldn’t be complete without stalls for our hairy hound friends. Pamper your pooch with a dogs body massage, dog wash, photo shoot, treats from the De-Pet and the Canine Wellness Kitchen or new gear from Pet Haus and Found & Hound. Second release tickets are on sale now — VIP tickets (showbag, meet and greet opportunities and VIP area): $81.60 Adult: $39.80 Child (under 12): $13.30 Concession: $23.50 Dogs and children under 4 are free.
            ', 'Dogapalooza Melbourne', '$39.80', '0488 333 099', 'Burnley Park, Yarra Boulevard, Burnley', 'http://www.facebook.com/DogapaloozaMelbourne', '11/12/2017', '11/12/2017', '5:00 PM', '11:00 AM', 'Dogapalooza - Dog Music Festival', '{/media/events/9573680421.jpg}', '2017-11-14 05:03:50.479779+00');


--
-- Data for Name: ruff_hikereviews; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--

INSERT INTO ruff_hikereviews (id, "user", comments, rating, hike_id, date, photo) VALUES (2, 'ano', 'res', 3, 2, '2017-11-21 12:07:02.562+00', '{}');


--
-- Data for Name: ruff_hikes; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--

INSERT INTO ruff_hikes (id, location, name, description, difficulty, distance, coordinates, more_info_link, leash, days_of_week, photo, date) VALUES (2, 'Little River, VIC, 3211, Australia', 'After Game', '“Enjoy this gentle loop walk with some medium gradients, starting and finishing at the Turntable car park. Rewarding views of the You Yangs and the surrounding area can be seen. The distinctive granite peaks of the You Yangs rise from the flat volcanic plains between Melbourne and Geelong. The You Yangs is a fantastic park for hiking and mountain biking. It has defined walking trails, horse trails and two mountain biking areas with over 50 kilometres of exciting trails catering for riders of all ages and ability. The park is also popular for rock climbing, its magnificent views, birdlife and for bush walks and picnics.”', 'Medium', 4, '-37.9368044, 144.4561033', 'Little River, VIC, 3211, Australia', true, '(M, T, W, T, F, S, S: 10am - 5pm)', '{/media/hikes/8132746950.jpeg,/media/hikes/4873695120.jpeg}', '2017-11-14 05:03:50.913867+00');
INSERT INTO ruff_hikes (id, location, name, description, difficulty, distance, coordinates, more_info_link, leash, days_of_week, photo, date) VALUES (5, 'Sydney, New South Wales, Australia', 'Test', '“Enjoy this gentle loop walk with some medium gradients, starting and finishing at the Turntable car park. Rewarding views of the You Yangs and the surrounding area can be seen. The distinctive granite peaks of the You Yangs rise from the flat volcanic plains between Melbourne and Geelong. The You Yangs is a fantastic park for hiking and mountain biking. It has defined walking trails, horse trails and two mountain biking areas with over 50 kilometres of exciting trails catering for riders of all ages and ability. The park is also popular for rock climbing, its magnificent views, birdlife and for bush walks and picnics.”
            ', 'Medium', 4, '-33.8688197, 151.20929550000005', 'Sydney, New South Wales, Australia', false, '(M, T, W, T, F, S, S: 10am - 5pm)
            ', '{/media/hikes/8132746950.jpeg,/media/hikes/4873695120.jpeg}', '2017-11-14 05:03:50.913867+00');


--
-- Data for Name: ruff_parkreviews; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--

INSERT INTO ruff_parkreviews (id, "user", comments, rating, park_id, date, photo) VALUES (1, 'Abi', 'rest', 3, 2, '2017-11-21 12:05:46.743759+00', '{}');


--
-- Data for Name: ruff_parks; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--

INSERT INTO ruff_parks (id, location, name, description, leash, days_of_week, photo, date) VALUES (2, 'Williams Road, South Yarra, VIC 3141', 'Como Park', 'Como Park is the local park for South Yarra residents
            ', true, 'M - F: 10am - 2pm
Weekends :10am - 2pm
            ', '{/media/parks/3795041268.jpg}', '2017-11-14 05:03:51.240125+00');
INSERT INTO ruff_parks (id, location, name, description, leash, days_of_week, photo, date) VALUES (15, 'Caulfield Park, Balaclava Road, Caulfield North, Victoria, Australia', 'Caulfield Park', 'Caulfield Park was once known as Paddy’s Swamp.

There are no records of when Paddy’s Swamp became known as Caulfield Park. In 1866 Paddy’s Swamp was reserved by legislation as a public park and watering place and by 1914, the area was permanently reserved for recreation.

It has been suggested that this name may stem from a dog owned by the Caulfield Roads Boards second chairman, William Murray Ross or from a livestock agent’s spotter, who frequented the drover camp.', true, 'M,T,W,T,F,S,S', '{/media/parks/7516483290.jpg}', '2017-11-29 12:39:56.375183+00');


--
-- Data for Name: ruff_restaurantreviews; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--

INSERT INTO ruff_restaurantreviews (id, "user", comments, rating, restaurant_id, date, photo) VALUES (2, 'Rest', 'Test', 3, 1, '2017-11-21 11:48:14.930001+00', '{}');


--
-- Data for Name: ruff_restaurants; Type: TABLE DATA; Schema: ruffguide; Owner: ruffdbadmin
--

INSERT INTO ruff_restaurants (id, type, name, description, phone_number, building_name, postcode, state, street_name, street_number, suburb, days_of_week, photo, date) VALUES (1, 'Bar', 'Great Northern Hotel', 'Laid-back 1880s watering hole with a bustling beer garden, serving classic pub grub and craft beers.', '9380 9569', '', '3054', 'VIC', 'Rathdowne St', 844, 'Carlton North', '(M, T, W, T, F, S, S: 10am - 5pm)', '{/media/restaurants/7192856430.jpg}', '2017-11-14 05:03:51.564156+00');


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('auth_permission_id_seq', 51, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('auth_user_id_seq', 1, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('django_content_type_id_seq', 17, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('django_migrations_id_seq', 33, true);


--
-- Name: ruff_events_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('ruff_events_id_seq', 13, true);


--
-- Name: ruff_hikereviews_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('ruff_hikereviews_id_seq', 3, true);


--
-- Name: ruff_hikes_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('ruff_hikes_id_seq', 5, true);


--
-- Name: ruff_parkreviews_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('ruff_parkreviews_id_seq', 2, true);


--
-- Name: ruff_parks_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('ruff_parks_id_seq', 15, true);


--
-- Name: ruff_restaurantreviews_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('ruff_restaurantreviews_id_seq', 2, true);


--
-- Name: ruff_restaurants_id_seq; Type: SEQUENCE SET; Schema: ruffguide; Owner: ruffdbadmin
--

SELECT pg_catalog.setval('ruff_restaurants_id_seq', 4, true);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: ruff_events ruff_events_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_events
    ADD CONSTRAINT ruff_events_pkey PRIMARY KEY (id);


--
-- Name: ruff_hikereviews ruff_hikereviews_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_hikereviews
    ADD CONSTRAINT ruff_hikereviews_pkey PRIMARY KEY (id);


--
-- Name: ruff_hikes ruff_hikes_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_hikes
    ADD CONSTRAINT ruff_hikes_pkey PRIMARY KEY (id);


--
-- Name: ruff_parkreviews ruff_parkreviews_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_parkreviews
    ADD CONSTRAINT ruff_parkreviews_pkey PRIMARY KEY (id);


--
-- Name: ruff_parks ruff_parks_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_parks
    ADD CONSTRAINT ruff_parks_pkey PRIMARY KEY (id);


--
-- Name: ruff_restaurantreviews ruff_restaurantreviews_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_restaurantreviews
    ADD CONSTRAINT ruff_restaurantreviews_pkey PRIMARY KEY (id);


--
-- Name: ruff_restaurants ruff_restaurants_pkey; Type: CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_restaurants
    ADD CONSTRAINT ruff_restaurants_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX auth_user_groups_group_id_97559544 ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX django_session_expire_date_a5c62663 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: ruff_hikereviews_hike_id_19b5d18c; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX ruff_hikereviews_hike_id_19b5d18c ON ruff_hikereviews USING btree (hike_id);


--
-- Name: ruff_parkreviews_park_id_dfa3aa61; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX ruff_parkreviews_park_id_dfa3aa61 ON ruff_parkreviews USING btree (park_id);


--
-- Name: ruff_restaurantreviews_restaurant_id_4c8cbded; Type: INDEX; Schema: ruffguide; Owner: ruffdbadmin
--

CREATE INDEX ruff_restaurantreviews_restaurant_id_4c8cbded ON ruff_restaurantreviews USING btree (restaurant_id);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ruff_hikereviews ruff_hikereviews_hike_id_19b5d18c_fk_ruff_hikes_id; Type: FK CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_hikereviews
    ADD CONSTRAINT ruff_hikereviews_hike_id_19b5d18c_fk_ruff_hikes_id FOREIGN KEY (hike_id) REFERENCES ruff_hikes(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ruff_parkreviews ruff_parkreviews_park_id_dfa3aa61_fk_ruff_parks_id; Type: FK CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_parkreviews
    ADD CONSTRAINT ruff_parkreviews_park_id_dfa3aa61_fk_ruff_parks_id FOREIGN KEY (park_id) REFERENCES ruff_parks(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ruff_restaurantreviews ruff_restaurantrevie_restaurant_id_4c8cbded_fk_ruff_rest; Type: FK CONSTRAINT; Schema: ruffguide; Owner: ruffdbadmin
--

ALTER TABLE ONLY ruff_restaurantreviews
    ADD CONSTRAINT ruff_restaurantrevie_restaurant_id_4c8cbded_fk_ruff_rest FOREIGN KEY (restaurant_id) REFERENCES ruff_restaurants(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

