from django.apps import AppConfig


class RuffConfig(AppConfig):
    name = 'ruff'
