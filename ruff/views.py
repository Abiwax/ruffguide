from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .forms import *
from django.http import JsonResponse, HttpResponse
from dateutil import parser
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import requests

from .models import *
import random, os, json
from django.conf import settings

media_path = settings.MEDIA_ROOT + '/'

# Main page view
def index(request):
    return render(request, 'ruff/index.html')

# View for news page
def news(request):
    return render(request, 'ruff/news.html')

#View for rendering about page
def about(request):
    return render(request, 'ruff/about.html')

'''
    All event related pages, pages with @login_required means Admin is required to login
'''
@login_required
def event_details(request):
    events = Events.get("date")
    return render(request, 'ruff/event_details.html', {"events": events})

'''
    Main event page, loads the list of events and changes the date to a UI acceptable format.
'''
def event(request):
    events = list(Events.get("name"))
    event_list = []
    for event in events:
        event.date = parser.parse(event.start_date)
        event_list.append(event)
    types = [{"value": event.type, "checked": False} for event in event_list]
    types = list({value['value']: value for value in types}.values())
    paginator = Paginator(event_list, 10)  # Show 10 events per page

    page = request.GET.get('page')
    try:
        events = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        events = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        events = paginator.page(paginator.num_pages)
    return render(request, 'ruff/event.html', {"events": events, "selected": "name", "date_selected": "any", "event_selected": "none", "types": types})

'''
    Filter events based on date type selected.
'''
def event_date_filter(request):
    try:
        date_selected = request.POST["date_selected"] if "date_selected" in request.POST else "any"
        selected = request.POST["selected"] if "selected" in request.POST else "name"
        try:
            events = list(Events.date_filter(date_selected, selected))
        except:
            events = []
        event_list = []
        for event in events:
            event.date = parser.parse(event.start_date)
            event_list.append(event)

        types = [{"value": event.type, "checked": False} for event in event_list]
        types = list({value['value']: value for value in types}.values())

        paginator = Paginator(event_list, 10)  # Show 10 events per page

        page = request.GET.get('page')
        try:
            events = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            events = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            events = paginator.page(paginator.num_pages)
        return render(request, 'ruff/event_view.html', {"events": events, "selected": selected, "date_selected": date_selected, "types": types})
    except:
        return HttpResponse("An error occured, please try again.", status=500)

'''
    Filter events based on vaious parameters(event type, date selected and selected sorting type)
'''
def event_filter(request):
    try:
        check_list =  json.loads(request.POST["check_list"]) if "check_list" in request.POST else []
        all_checks =  json.loads(request.POST["all_checks"]) if "all_checks" in request.POST else check_list
        date_selected =  request.POST["date_selected"] if "date_selected" in request.POST else "any"
        selected =  request.POST["selected"] if "selected" in request.POST else "name"
        if(len(check_list) == 0):
            events = list(Events.get(selected))
        else:
            try:
                events = list(Events.type_filter(check_list, selected))
            except:
                events = []

        event_list = []
        for event in events:
            event.date = parser.parse(event.start_date)
            event_list.append(event)
        types = []
        for type in all_checks:
            if type in check_list:
                types.append({"value": type, "checked": True})
            else:
                types.append({"value": type, "checked": False})
        types = list({value['value']:value for value in types}.values())
        paginator = Paginator(event_list, 10)  # Show 10 events per page

        page = request.GET.get('page')
        try:
            events = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            events = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            events = paginator.page(paginator.num_pages)
        return render(request, 'ruff/event_view.html', {"events": events, "selected": selected, "date_selected": date_selected, "types": types})
    except:
        return HttpResponse("An error occured, please try again.", status=500)

'''
   Check if user's location is stored in particular session.
'''
def location_stored(request):
    user_location = request.session.get('user_location')
    if user_location != None:
        return JsonResponse({'location': True})

    return JsonResponse({'location': False})

'''
    Calculate distance between two locations
'''
def getDistance(location, park_location):
    origin = str(location["latitude"]) + "," + str(location["longitude"]) if type(location) is dict else location
    distance_km = None
    matrixurl = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + origin + "&destinations=" + park_location + "&key=AIzaSyDemTKrKvU6qAmvZIvC-J4JU5na3C-mGV4"
    matrixinfo = requests.post(matrixurl)
    matrixresult = json.loads(matrixinfo.text)
    if 'rows' in matrixresult.keys() and len(matrixresult['rows']) > 0:
        matrixdistance = matrixresult['rows']
        # if result is not empty
        if matrixdistance[0]['elements'][0]['status'] != 'ZERO_RESULTS':
            # get the distance
            distance = matrixdistance[0]['elements'][0]['distance']['value']
            if (distance != ""):
                distance_km = distance / 1000
    return distance_km

'''
    Main hike page, loads the list of hikes.
'''
def hike(request):
    user_location = request.session.get('user_location')
    hike_list = list(Hikes.get("name"))
    if(user_location != None):
        location = json.loads(user_location)
        return render(request, 'ruff/hike.html', {"hikes": hike_index(request, location["latitude"], location["longitude"], hike_list), "selected": "name", "diff_selected": "all", "range_distance": 10, "leash_selected": True})
    else:
        paginator = Paginator(hike_list, 10)  # Show 10 hikes per page

        page = request.GET.get('page')
        try:
            hikes = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            hikes = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            hikes = paginator.page(paginator.num_pages)
        return render(request, 'ruff/hike.html', {"hikes": hikes, "selected": "name", "diff_selected": "all", "range_distance": 10, "leash_selected": True})

'''
    get list of hikes with distanceaway away from selected location and destination location
'''
def hike_index(request, lat, lng, h_list=None):
    request.session['user_location'] = json.dumps({"latitude": lat, "longitude": lng})
    hikes_list = list(Hikes.get("name")) if h_list == None else h_list
    hike_list = []
    for hike in hikes_list:
        try:
            matrixurl = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + str(lat) + "," + str(lng) + "&destinations=" + hike.location + "&key=AIzaSyDemTKrKvU6qAmvZIvC-J4JU5na3C-mGV4"
            matrixinfo = requests.post(matrixurl)
            matrixresult = json.loads(matrixinfo.text)
            if 'rows' in matrixresult.keys() and len(matrixresult['rows']) > 0:
                matrixdistance = matrixresult['rows']
                # if result is not empty
                if matrixdistance[0]['elements'][0]['status'] != 'ZERO_RESULTS':
                    # get the distance
                    distance = matrixdistance[0]['elements'][0]['distance']['value']
                    if(distance != ""):
                        distance_km = distance / 1000
                        hike.distanceaway = str(round(distance_km)) + "km"
            hike_list.append(hike)
        except:
            hike_list.append(hike)
            pass

    paginator = Paginator(hikes_list, 10)  # Show 10 hikes per page
    page = request.GET.get('page')
    try:
        hikes = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        hikes = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        hikes = paginator.page(paginator.num_pages)
    return hikes

'''
    get list of hikes with distanceaway away from selected location and destination location
'''
def hike_location(request):
    try:
        hike_list = list(Hikes.get("name"))

        latitude = request.POST["latitude"]
        longitude = request.POST["longitude"]

        return render(request, 'ruff/hike_view.html',
                  {"hikes": hike_index(request, latitude, longitude, hike_list),
                   "selected": "name", "range_distance": 10, "leash_selected": True, "diff_selected": "all"})
    except Exception as e:
        print(e)
        return HttpResponse("An error occured, please try again.", status=500)

'''
    filter list of hikes based on several parameters
'''
def hike_distance(request):
    try:
        address = request.POST["address"]
        range = request.POST["range"]
        selected = request.POST["selected"]
        diff_selected = request.POST["diff_selected"]
        leash = False if request.POST["off_leash"] == "false" else True

        try:
            hikes_list = list(Hikes.filter(leash, selected, diff_selected))
        except:
            hikes_list = []

        hike_list = []
        for hike in hikes_list:
            try:
                if (address != ""):
                    distance_km = getDistance(address, hike.location)
                    if distance_km != None:
                        if (float(distance_km) <= float(range)) or float(range) == float(100):
                            hike.distanceaway = str(round(distance_km)) + "km"
                            hike_list.append(hike)
                else:
                    user_location = request.session.get('user_location')
                    if (user_location != None):
                        location = json.loads(user_location)
                        distance_km = getDistance(location, hike.location)
                        if distance_km != None:
                            hike.distanceaway = str(round(distance_km)) + "km"
                    hike_list.append(hike)

            except Exception as e:
                pass

        paginator = Paginator(hike_list, 10)  # Show 10 hikes per page

        page = request.GET.get('page')
        try:
            hikes = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            hikes = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            hikes = paginator.page(paginator.num_pages)
        return render(request, 'ruff/hike_view.html', {"hikes": hikes, "selected": selected, "diff_selected": diff_selected, "leash_selected": leash, "range_distance": range, "location": address})
    except Exception as e:
        print(e)
        return HttpResponse("An error occured, please try again.", status=500)

'''
    Main park page, loads the list of hikes.
'''
def park(request):
    user_location = request.session.get('user_location')
    park_list = list(Parks.get("name"))
    if (user_location != None):
        location = json.loads(user_location)
        return render(request, 'ruff/park.html',
                      {"parks": park_index(request, location["latitude"], location["longitude"], park_list),
                       "selected": "name", "range_distance": 10, "leash_selected": True})
    else:
        paginator = Paginator(park_list, 10)  # Show 10 parks per page

        page = request.GET.get('page')
        try:
            parks = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            parks = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            parks = paginator.page(paginator.num_pages)
        return render(request, 'ruff/park.html', {"parks": parks, "selected": "name", "range_distance": 10, "leash_selected": True})

'''
    get list of parks with distanceaway away from selected location and destination location
'''
def park_location(request):
    try:
        park_list = list(Parks.get("name"))

        latitude = request.POST["latitude"]
        longitude = request.POST["longitude"]

        return render(request, 'ruff/park_view.html',
                  {"parks": park_index(request, latitude, longitude, park_list),
                   "selected": "name", "range_distance": 10, "leash_selected": True})
    except:
        return HttpResponse("An error occured, please try again.", status=500)

'''
    get list of parks with distanceaway away from selected location and destination location
'''
def park_index(request, lat, lng, p_list=None):
    request.session['user_location'] = json.dumps({"latitude": lat, "longitude": lng})
    parks_list = list(Parks.get("name")) if p_list == None else p_list

    park_list = []
    for park in parks_list:
        try:
            matrixurl = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + str(lat) + "," + str(
                lng) + "&destinations=" + park.location + "&key=AIzaSyDemTKrKvU6qAmvZIvC-J4JU5na3C-mGV4"
            matrixinfo = requests.post(matrixurl)
            matrixresult = json.loads(matrixinfo.text)
            if 'rows' in matrixresult.keys() and len(matrixresult['rows']) > 0:
                matrixdistance = matrixresult['rows']
                # if result is not empty
                if matrixdistance[0]['elements'][0]['status'] != 'ZERO_RESULTS':
                    # get the distance
                    distance = matrixdistance[0]['elements'][0]['distance']['value']
                    if (distance != ""):
                        distance_km = distance / 1000
                        park.distanceaway = str(round(distance_km)) + "km"
            park_list.append(park)
        except:
            park_list.append(park)
            pass

    paginator = Paginator(park_list, 10)  # Show 10 parks per page

    page = request.GET.get('page')
    try:
        parks = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        parks = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        parks = paginator.page(paginator.num_pages)
    return parks

'''
    filter list of parks based on several parameters
'''
def park_distance(request):
    try:
        address = request.POST["address"]
        range = request.POST["range"]
        selected = request.POST["selected"]
        leash = False if request.POST["off_leash"] == "false" else True

        try:
            parks_list = list(Parks.filter(leash, selected))
        except:
            parks_list = []

        park_list = []
        for park in parks_list:
            try:
                if (address != ""):
                    distance_km = getDistance(address, park.location)
                    if distance_km != None:
                        if (float(distance_km) <= float(range)) or float(range) == float(100):
                            park.distanceaway = str(round(distance_km)) + "km"
                            park_list.append(park)
                else:
                    user_location = request.session.get('user_location')
                    if (user_location != None):
                        location = json.loads(user_location)
                        distance_km = getDistance(location, park.location)
                        if distance_km != None:
                            park.distanceaway = str(round(distance_km)) + "km"
                    park_list.append(park)

            except:
                pass

        paginator = Paginator(park_list, 10)  # Show 10 parks per page

        page = request.GET.get('page')
        try:
            parks = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            parks = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            parks = paginator.page(paginator.num_pages)

        return render(request, 'ruff/park_view.html', {"parks": parks, "selected": selected, "leash_selected": leash, "range_distance": range, "location": address})
    except:
        return HttpResponse("An error occured, please try again.", status=500)

'''
    Main restaurant page, loads the list of hikes.
'''
def restaurant(request):
    try:
        user_location = request.session.get('user_location')
        restaurant_list = list(Restaurants.get("name"))
        if (user_location != None):
            location = json.loads(user_location)
            return render(request, 'ruff/restaurant.html',
                          {"restaurants": restaurant_index(request, location["latitude"], location["longitude"], restaurant_list),
                           "selected": "name", "range_distance": 10})
        else:
            paginator = Paginator(restaurant_list, 10)  # Show 10 restaurants per page

            page = request.GET.get('page')
            try:
                restaurants = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                restaurants = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                restaurants = paginator.page(paginator.num_pages)
            return render(request, 'ruff/restaurant.html', {"restaurants": restaurants, "selected": "name"})
    except:
        return HttpResponse("An error occured, please try again.", status=500)

'''
    get list of restaurants with distanceaway away from selected location and destination location
'''
def restaurant_location(request):
    try:
        restaurant_list = list(Restaurants.get("name"))

        latitude = request.POST["latitude"]
        longitude = request.POST["longitude"]

        return render(request, 'ruff/restaurant_view.html',
                      {"restaurants": restaurant_index(request, latitude, longitude, restaurant_list),
                       "selected": "name", "range_distance": 10})
    except:
        return HttpResponse("An error occured, please try again.", status=500)

'''
    get list of restaurants with distanceaway away from selected location and destination location
'''
def restaurant_index(request, lat, lng, r_list=None):
    request.session['user_location'] = json.dumps({"latitude": lat, "longitude": lng})
    restaurants_list = list(Restaurants.get("name")) if r_list == None else r_list

    restaurant_list = []
    for restaurant in restaurants_list:
        try:
            matrixurl = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + str(lat) + "," + str(
                lng) + "&destinations=" + str(restaurant.street_number) + ", " + restaurant.street_name + " " + " " + restaurant.suburb + " " + restaurant.state + "&key=AIzaSyDemTKrKvU6qAmvZIvC-J4JU5na3C-mGV4"
            matrixinfo = requests.post(matrixurl)
            matrixresult = json.loads(matrixinfo.text)
            if 'rows' in matrixresult.keys() and len(matrixresult['rows']) > 0:
                matrixdistance = matrixresult['rows']
                # if result is not empty
                if matrixdistance[0]['elements'][0]['status'] != 'ZERO_RESULTS':
                    # get the distance
                    distance = matrixdistance[0]['elements'][0]['distance']['value']
                    if (distance != ""):
                        distance_km = distance / 1000
                        restaurant.distanceaway = str(round(distance_km)) + "km"
            restaurant_list.append(restaurant)
        except:
            restaurant_list.append(restaurant)
            pass

    paginator = Paginator(restaurant_list, 10)  # Show 10 restaurants per page

    page = request.GET.get('page')
    try:
        restaurants = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        restaurants = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        restaurants = paginator.page(paginator.num_pages)
    return restaurants

'''
    filter list of restaurants based on several parameters
'''
def restaurant_distance(request):
    try:
        address = request.POST["address"]
        print(address)
        range = request.POST["range"]
        selected = request.POST["selected"]

        try:
            restaurants_list = list(Restaurants.get(selected))
        except:
            restaurants_list = []

        restaurant_list = []
        for restaurant in restaurants_list:
            try:
                res_location = str(restaurant.street_number) + ", " + restaurant.street_name + " " + " " + restaurant.suburb + " " + restaurant.state
                if (address != ""):
                    distance_km = getDistance(address, res_location)
                    if distance_km != None:
                        if (float(distance_km) <= float(range)) or float(range) == float(100):
                            restaurant.distanceaway = str(round(distance_km)) + "km"
                            restaurant_list.append(restaurant)
                else:
                    user_location = request.session.get('user_location')
                    if (user_location != None):
                        location = json.loads(user_location)
                        distance_km = getDistance(location, res_location)
                        if distance_km != None:
                            restaurant.distanceaway = str(round(distance_km)) + "km"
                    restaurant_list.append(restaurant)
            except Exception as e:
                pass
        paginator = Paginator(restaurant_list, 10)  # Show 10 restaurants per page

        page = request.GET.get('page')
        try:
            restaurants = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            restaurants = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            restaurants = paginator.page(paginator.num_pages)
        return render(request, 'ruff/restaurant_view.html', {"restaurants": restaurants, "selected": selected, "range_distance": range, "location": address})
    except:
        return HttpResponse("An error occured, please try again.", status=500)

''' Views for single items. '''
def view_single_event(request, event_id):
    events = Events.get_single(event_id)
    return render(request, 'ruff/single_event.html', {"event": events})

def view_single_restaurant(request, restaurant_id):
    restaurants = Restaurants.get_single(restaurant_id)
    for rev in restaurants["review"]:
        rev.photo_list = grouped(rev.photo, 10)
        rev.photo = json.dumps(rev.photo)
    return render(request, 'ruff/single_restaurant.html', {"restaurant": restaurants})

def view_single_park(request, park_id):
    parks = Parks.get_single(park_id)
    for rev in parks["review"]:
        rev.photo_list = grouped(rev.photo, 10)
        rev.photo = json.dumps(rev.photo)
    return render(request, 'ruff/single_park.html', {"park": parks})

def view_single_hike(request, hike_id):
    hikes = Hikes.get_single(hike_id)
    for rev in hikes["review"]:
        rev.photo_list = grouped(rev.photo, 10)
        rev.photo = json.dumps(rev.photo)
    return render(request, 'ruff/single_hike.html', {"hike": hikes})

'''Views for review forms'''
def restaurant_review_form(request, restaurant_id):
    try:
        if request.POST:
            form = Restaurant_Review_Form(request.POST)
            review_detail = request.POST.copy()
            if form.is_valid():
                list_of_photos = request.FILES.getlist('photos')
                file_save_path = media_path + "restaurants/reviews/"
                make_dir(file_save_path)
                url_path = "/media/restaurants/reviews/"
                review_detail["photo"] = save_image_file(list_of_photos, url_path, file_save_path) if len(
                    list_of_photos) > 0 else []
                review_detail["rating"] = float(review_detail["rating"])
                RestaurantReviews.create(review_detail, restaurant_id)
                restaurants = Restaurants.get_single(int(restaurant_id))
                for rev in restaurants["review"]:
                    rev.photo_list = grouped(rev.photo, 10)
                    rev.photo = json.dumps(rev.photo)

                return render(request, 'ruff/restaurant_reviews.html', {"restaurant": restaurants})
            else:
                return HttpResponse("One or more fields are empty.", status=403)

    except Exception as e:
        return HttpResponse("An error occured, please try again.", status=500)

def hike_review_form(request, hike_id):
    try:
        if request.POST:
            print(request.POST)
            form = Hike_Review_Form(request.POST)
            review_detail = request.POST.copy()
            if form.is_valid():
                list_of_photos = request.FILES.getlist('photos')
                file_save_path = media_path + "hikes/reviews/"
                make_dir(file_save_path)
                url_path = "/media/hikes/reviews/"
                review_detail["photo"] = save_image_file(list_of_photos, url_path, file_save_path) if len(
                    list_of_photos) > 0 else []
                review_detail["rating"] = float(review_detail["rating"])
                HikeReviews.create(review_detail, hike_id)
                hikes = Hikes.get_single(int(hike_id))
                for rev in hikes["review"]:
                    rev.photo_list = grouped(rev.photo, 10)
                    rev.photo = json.dumps(rev.photo)

                return render(request, 'ruff/hike_reviews.html', {"hike": hikes})
            else:
                return HttpResponse("One or more fields are empty.", status=403)

    except Exception as e:
        return HttpResponse("An error occured, please try again.", status=500)

def park_review_form(request, park_id):
    try:
        if request.POST:
            form = Park_Review_Form(request.POST)
            review_detail = request.POST.copy()
            if form.is_valid():
                list_of_photos = request.FILES.getlist('photos')
                file_save_path = media_path + "parks/reviews/"
                make_dir(file_save_path)
                url_path = "/media/parks/reviews/"
                review_detail["photo"] = save_image_file(list_of_photos, url_path, file_save_path) if len(
                    list_of_photos) > 0 else []
                review_detail["rating"] = float(review_detail["rating"])
                ParkReviews.create(review_detail, park_id)
                parks = Parks.get_single(int(park_id))
                for rev in parks["review"]:
                    rev.photo_list = grouped(rev.photo, 10)
                    rev.photo = json.dumps(rev.photo)

                return render(request, 'ruff/park_reviews.html', {"park": parks})
            else:
                return HttpResponse("One or more fields are empty.", status=403)

    except Exception as e:
        return HttpResponse("An error occured, please try again.", status=500)

'''Main admin pages for hikes, parks, events and restaurants. '''
@login_required
def hike_details(request):
    hikes = Hikes.get("name")
    return render(request, 'ruff/hike_details.html', {"hikes": hikes})

@login_required
def park_details(request):
    parks = Parks.get("name")
    return render(request, 'ruff/park_details.html', {"parks": parks})

@login_required
def restaurant_details(request):
    restaurants = Restaurants.get("name")
    return render(request, 'ruff/restaurant_details.html', {"restaurants": restaurants})

'''Admin pages for deleting hike, park, event and restaurant details and reviews. '''
@login_required
def delete_event_detail(request, event_id):
    events = Events.get_single(event_id).photo
    delete_list(events, "events/")
    Events.delete_single(event_id)
    events = Events.get("name")
    return render(request, 'ruff/event_details.html', {"events": events})

@login_required
def delete_hike_detail(request, hike_id):
    hikes = Hikes.get_single(hike_id)["single_hike"].photo
    delete_list(hikes, "hikes/")
    Hikes.delete_single(hike_id)
    hikes = Hikes.get("name")
    return render(request, 'ruff/hike_details.html', {"hikes": hikes})

@login_required
def delete_hike_review(request, hike_id, review_id):
    try:
        reviews = Hikes.get_single(hike_id)["review"]
        for review in reviews:
            review_photo = review.photo
            delete_list(review_photo, "hikes/reviews/")
        HikeReviews.delete_single(review_id)
        return render(request, 'ruff/hike_form.html', {"form": single_hike(hike_id)["single_hike"], "reviews": single_hike(hike_id)["review"]})
    except Exception as e:
        return render(request, 'ruff/hike_form.html', {"error_message":"An error occured, " + str(e),"form": single_hike(hike_id)["single_hike"], "reviews": single_hike(hike_id)["review"]})

@login_required
def delete_park_detail(request, park_id):
    parks = Parks.get_single(park_id)["single_park"].photo
    delete_list(parks, "parks/")
    Parks.delete_single(park_id)
    parks = Parks.get("name")
    return render(request, 'ruff/park_details.html', {"parks": parks})

@login_required
def delete_park_review(request, park_id, review_id):
    try:
        reviews = Parks.get_single(park_id)["review"]
        for review in reviews:
            review_photo = review.photo
            delete_list(review_photo, "parks/reviews/")
        ParkReviews.delete_single(review_id)
        return render(request, 'ruff/park_form.html', {"form": single_park(park_id)["single_park"], "reviews": single_park(park_id)["review"]})
    except Exception as e:
        return render(request, 'ruff/park_form.html', {"error_message":"An error occured, " + str(e),"form": single_park(park_id)["single_park"], "reviews": single_park(park_id)["review"]})

@login_required
def delete_restaurant_detail(request, restaurant_id):
    restaurants = Restaurants.get_single(restaurant_id)["single_restaurant"].photo
    delete_list(restaurants, "restaurants/")
    Restaurants.delete_single(restaurant_id)
    restaurants = Restaurants.get("name")
    return render(request, 'ruff/restaurant_details.html', {"restaurants": restaurants})

@login_required
def delete_restaurant_review(request, restaurant_id, review_id):
    try:
        reviews = Restaurants.get_single(restaurant_id)["review"]
        for review in reviews:
            review_photo = review.photo
            delete_list(review_photo, "restaurants/reviews/")
        RestaurantReviews.delete_single(review_id)
        return render(request, 'ruff/restaurant_form.html', {"form": single_restaurant(restaurant_id)["single_restaurant"], "reviews": single_restaurant(restaurant_id)["review"]})
    except Exception as e:
        return render(request, 'ruff/restaurant_form.html', {"error_message":"An error occured, " + str(e),"form": single_restaurant(restaurant_id)["single_restaurant"], "reviews": single_restaurant(restaurant_id)["review"]})

''' Admin pages for updating hike, park, event and restaurant details. '''
@login_required
def update_event_detail(request, event_id):
    events = Events.get_single(event_id)
    events.update = "checked"
    events.photo_list = grouped(events.photo, 10)
    events.photo = json.dumps(events.photo)
    return render(request, 'ruff/event_form.html', {"form": events})

@login_required
def update_hike_detail(request, hike_id):
    hikes = Hikes.get_single(hike_id)
    hikes["single_hike"].photo_list = grouped(hikes["single_hike"].photo, 10)
    hikes["single_hike"].photo = json.dumps(hikes["single_hike"].photo)
    hikes["single_hike"].update = "checked"
    return render(request, 'ruff/hike_form.html', {"form": hikes["single_hike"]})

@login_required
def update_park_detail(request, park_id):
    parks = Parks.get_single(park_id)
    parks["single_park"].update = "checked"
    parks["single_park"].photo_list = grouped(parks["single_park"].photo, 10)
    parks["single_park"].photo = json.dumps(parks["single_park"].photo)
    return render(request, 'ruff/park_form.html', {"form": parks["single_park"]})

@login_required
def update_restaurant_detail(request, restaurant_id):
    restaurants = Restaurants.get_single(restaurant_id)
    restaurants["single_restaurant"].update = "checked"
    restaurants["single_restaurant"].photo_list = grouped(restaurants["single_restaurant"].photo, 10)
    restaurants["single_restaurant"].photo = json.dumps(restaurants["single_restaurant"].photo)
    return render(request, 'ruff/restaurant_form.html', {"form": restaurants["single_restaurant"]})

''' Functions for reformatting photos for parks, restaurants and hikes for reviews. '''
def single_hike(hike_id):
    hikes = Hikes.get_single(hike_id)
    hikes["single_hike"].photo_list = grouped(hikes["single_hike"].photo, 10)
    hikes["single_hike"].disabled = "disabled"
    for rev in hikes["review"]:
        rev.photo_list = grouped(rev.photo, 10)
        rev.photo = json.dumps(rev.photo)
    return hikes

def single_park(park_id):
    parks = Parks.get_single(park_id)
    parks["single_park"].disabled = "disabled"
    parks["single_park"].photo_list = grouped(parks["single_park"].photo, 10)
    for rev in parks["review"]:
        rev.photo_list = grouped(rev.photo, 10)
        rev.photo = json.dumps(rev.photo)
    return parks

def single_restaurant(restaurant_id):
    restaurants = Restaurants.get_single(restaurant_id)
    restaurants["single_restaurant"].disabled = "disabled"
    restaurants["single_restaurant"].photo_list = grouped(restaurants["single_restaurant"].photo, 10)
    for rev in restaurants["review"]:
        rev.photo_list = grouped(rev.photo, 10)
        rev.photo = json.dumps(rev.photo)
    return restaurants

''' Admin view for single event, parks, hikes and restaurants detail. '''
@login_required
def view_event_detail(request, event_id):
    events = Events.get_single(event_id)
    events.disabled = "disabled"
    events.photo_list = grouped(events.photo, 10)
    return render(request, 'ruff/event_form.html', {"form": events})

@login_required
def view_hike_detail(request, hike_id):
    return render(request, 'ruff/hike_form.html', {"form": single_hike(hike_id)["single_hike"], "reviews": single_hike(hike_id)["review"]})

@login_required
def view_park_detail(request, park_id):
    return render(request, 'ruff/park_form.html', {"form": single_park(park_id)["single_park"], "reviews": single_park(park_id)["review"]})

@login_required
def view_restaurant_detail(request, restaurant_id):
    return render(request, 'ruff/restaurant_form.html', {"form": single_restaurant(restaurant_id)["single_restaurant"], "reviews": single_restaurant(restaurant_id)["review"]})

''' Admin view for event, parks, hikes and restaurants forms. '''
@login_required
def event_form(request):
    try:
        if 'event-submit' in request.POST:
            form = EventsForm(request.POST)
            event_detail = request.POST.copy()
            if form.is_valid():
                list_of_photos = request.FILES.getlist('photos')
                file_save_path = media_path + "events/"
                make_dir(file_save_path)
                # url_path = request.META['HTTP_ORIGIN'] + "/media/events/"
                url_path = "/media/events/"
                if ("update" in event_detail):
                    old_list = json.loads(event_detail["photo"])
                    event_detail["photo"] = save_image_file(list_of_photos, url_path, file_save_path) if len(
                        list_of_photos) > 0 else json.loads(event_detail["photo"])
                    if len(list_of_photos) > 0:
                        delete_list(old_list, "events/")
                    Events.update(event_detail)
                else:
                    event_detail["photo"] = save_image_file(list_of_photos, url_path, file_save_path) if len(
                        list_of_photos) > 0 else []

                    Events.create(event_detail)

                events = Events.get("name")

                return render(request, 'ruff/event_details.html', {"events": events, "selected": "name"})
            else:
                if ("update" in event_detail):
                    return render(request, 'ruff/event_form.html', {
                        'error_message': "One or more fields are empty", 'form': event_detail})
                else:
                    return render(request, 'ruff/event_form.html', {
                        'error_message': "One or more fields are empty"})
        return render(
            request, 'ruff/event_form.html'
        )
    except Exception as e:
        print(e)
        return render(request, 'ruff/event_form.html', {
            'error_message': "There is an error",
        })

@login_required
def park_form(request):
    try:
        if 'park-submit' in request.POST:
            form = ParksForm(request.POST)
            park_detail = request.POST.copy()
            if form.is_valid():
                list_of_photos = request.FILES.getlist('photos')
                file_save_path = media_path + "parks/"
                make_dir(file_save_path)
                url_path = "/media/parks/"
                if ("update" in park_detail):
                    old_list = json.loads(park_detail["photo"])
                    park_detail["photo"] = save_image_file(list_of_photos, url_path, file_save_path) if len(
                        list_of_photos) > 0 else json.loads(park_detail["photo"])
                    if len(list_of_photos) > 0:
                        delete_list(old_list, "parks/")
                    Parks.update(park_detail)
                else:
                    park_detail["photo"] = save_image_file(list_of_photos, url_path, file_save_path) if len(
                        list_of_photos) > 0 else []

                    Parks.create(park_detail)

                parks = Parks.get("name")
                return render(request, 'ruff/park_details.html', {"parks": parks})
            else:
                if ("update" in park_detail):
                    return render(request, 'ruff/park_form.html', {
                        'error_message': "One or more fields are empty", 'form': park_detail})
                else:
                    return render(request, 'ruff/park_form.html', {
                        'error_message': "One or more fields are empty"})
        return render(
            request, 'ruff/park_form.html'
        )
    except Exception as e:
        print(e)
        return render(request, 'ruff/park_form.html', {
            'error_message': "There is an error",
        })

@login_required
def hike_form(request):
    try:
        if 'hike-submit' in request.POST:

            form = HikesForm(request.POST)
            hike_detail = request.POST.copy()
            if form.is_valid():
                list_of_photos = request.FILES.getlist('photos')
                file_save_path = media_path + "hikes/"
                make_dir(file_save_path)
                url_path = "/media/hikes/"
                if ("update" in hike_detail):
                    old_list = json.loads(hike_detail["photo"])
                    hike_detail["photo"] = save_image_file(list_of_photos, url_path, file_save_path) if len(
                        list_of_photos) > 0 else json.loads(hike_detail["photo"])
                    if len(list_of_photos) > 0:
                        delete_list(old_list, "hikes/")
                    Hikes.update(hike_detail)
                else:
                    hike_detail["photo"] = save_image_file(list_of_photos, url_path, file_save_path) if len(
                        list_of_photos) > 0 else []

                    Hikes.create(hike_detail)

                hikes = Hikes.get("name")

                return render(request, 'ruff/hike_details.html', {"hikes": hikes})
            else:
                if ("update" in hike_detail):
                    return render(request, 'ruff/hike_form.html', {
                        'error_message': "One or more fields are empty", 'form': hike_detail})
                else:
                    return render(request, 'ruff/hike_form.html', {
                        'error_message': "One or more fields are empty"})
        return render(
            request, 'ruff/hike_form.html'
        )
    except Exception as e:
        print(e)
        return render(request, 'ruff/hike_form.html', {
            'error_message': "There is an error",
        })

@login_required
def restaurant_form(request):
    try:
        if 'restaurant-submit' in request.POST:
            form = RestaurantsForm(request.POST)
            restaurant_detail = request.POST.copy()
            if form.is_valid():
                list_of_photos = request.FILES.getlist('photos')
                file_save_path = media_path + "restaurants/"
                make_dir(file_save_path)
                url_path = "/media/restaurants/"
                if ("update" in restaurant_detail):
                    old_list = json.loads(restaurant_detail["photo"])
                    restaurant_detail["photo"] = save_image_file(list_of_photos, url_path, file_save_path) if len(
                        list_of_photos) > 0 else json.loads(restaurant_detail["photo"])
                    if len(list_of_photos) > 0:
                        delete_list(old_list, "restaurants/")
                    Restaurants.update(restaurant_detail)
                else:
                    restaurant_detail["photo"] = save_image_file(list_of_photos, url_path, file_save_path) if len(
                        list_of_photos) > 0 else []

                    Restaurants.create(restaurant_detail)
                restaurants = Restaurants.get("name")

                return render(request, 'ruff/restaurant_details.html', {"restaurants": restaurants})
            else:
                if ("update" in restaurant_detail):
                    return render(request, 'ruff/restaurant_form.html', {
                        'error_message': "One or more fields are empty", 'form': restaurant_detail})
                else:
                    return render(request, 'ruff/restaurant_form.html', {
                        'error_message': "One or more fields are empty"})
        return render(
            request, 'ruff/restaurant_form.html'
        )
    except Exception as e:
        print(e)
        return render(request, 'ruff/restaurant_form.html', {
            'error_message': "An error occurred.",
        })

def make_dir(file_path):
    if not os.path.exists(file_path):
        os.mkdir(file_path)

''' Generate random number for use with saving pictures. '''
def randN():
    l = list(range(10)) # compat py2 & py3
    while l[0] == 0:
        random.shuffle(l)
    return int(''.join(str(d) for d in l[:10]))

''' Saving a list of images. '''
def save_image_file(file_list, url_path, save_path):
    image_list = []

    for file in file_list:
        file_name = str(randN())
        extension = str(file).rsplit(".")[1]
        file_path = save_path + file_name + "." + extension
        with open(file_path, 'wb+') as file_destination:
            file_destination.write(file.read())
        file_destination.close()
        image_list.append(url_path + file_name + "." + extension)
    return image_list

''' Delete a list of images '''
def delete_list(file_list, directory):
    for file in file_list:
        filename = file.split(directory)[1]
        try:
            os.remove(media_path+directory+filename)
        except OSError:
            pass

''' Group a list into different numbers. '''
def grouped(any_list, num):
    new_list = [list(any_list[i:i + num]) for i in range(0, len(any_list), num)]
    return new_list

'''
Handle error pages, does not work properly in heroku due to static files deployment
'''
def handler400(request):
    return render(request, 'ruff/400.html', status=400)

def handler403(request):
    return render(request, 'ruff/403.html', status=403)

def handler404(request):
    return render(request, 'ruff/404.html', status=404)

def handler500(request):
    return render(request, 'ruff/500.html', status=500)