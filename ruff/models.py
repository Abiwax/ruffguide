from django.db import models
from django.contrib.postgres.fields import JSONField, ArrayField
from django.db.models.functions import Extract
import datetime

@models.DateTimeField.register_lookup
class ExtractWeek(Extract):
    lookup_name = 'week'

# Create your models here.
class Events(models.Model):
    name = models.CharField(max_length=50, default="", null=False)
    type = models.CharField(max_length=50, default="", null=False)
    description = models.TextField(default="", null=False)
    organiser = models.CharField(max_length=100, default="", null=False)
    cost = models.TextField(default="", null=False)
    contact = models.CharField(max_length=200, default="")
    website = models.URLField(default="")
    date = models.DateTimeField(auto_now_add=True)
    location = models.CharField(max_length=200, default="", null=False)
    start_date = models.CharField(max_length=15, default="", null=False)
    end_date = models.CharField(max_length=15, default="", null=False)
    start_time = models.CharField(max_length=10, default="", null=False)
    end_time = models.CharField(max_length=10, default="", null=False)
    photo = ArrayField(models.URLField(default=""), default=[])

    @classmethod
    def create(cls, event_detail):
        event_save = cls(name=event_detail["name"], type=event_detail["type"], description=event_detail["description"], organiser=event_detail["organiser"], cost=event_detail["cost"], contact=event_detail["contact"], website=event_detail["website"], location=event_detail["location"], start_date=event_detail["start_date"], end_date=event_detail["end_date"], start_time=event_detail["start_time"], end_time=event_detail["end_time"], photo=event_detail["photo"])
        event_save.save()
        return event_save

    @classmethod
    def update(cls, event_detail):
        single_event = cls.objects.filter(id=event_detail["id"]).get()
        single_event.id = event_detail["id"]
        single_event.name=event_detail["name"]
        single_event.type=event_detail["type"]
        single_event.description=event_detail["description"]
        single_event.organiser=event_detail["organiser"]
        single_event.cost=event_detail["cost"]
        single_event.contact=event_detail["contact"]
        single_event.website=event_detail["website"]
        single_event.location=event_detail["location"]
        single_event.start_date=event_detail["start_date"]
        single_event.end_date=event_detail["end_date"]
        single_event.start_time=event_detail["start_time"]
        single_event.end_time=event_detail["end_time"]
        single_event.photo=event_detail["photo"]
        single_event.save()
        return single_event

    @classmethod
    def delete_single(cls, event_id):
        status = cls.objects.filter(id=event_id).delete()
        return {"status": status}

    @classmethod
    def get(cls, type):
        event_lists = cls.objects.all().order_by('-'+type)
        return event_lists

    @classmethod
    def date_filter(cls, date, type):
        if(date == "today"):
            if (type == "start_date" or type == "cost"):
                event_lists = cls.objects.raw("SELECT * FROM ruff_events WHERE  DATE(start_date) = DATE 'now' ORDER BY {0} DESC".format(type))
            else:
                event_lists = cls.objects.raw("SELECT * FROM ruff_events WHERE  DATE(start_date) = DATE 'now' ORDER BY {0} ASC".format(type))
        elif(date == "tomorrow"):
            if(type == "start_date" or type == "cost"):
                event_lists = cls.objects.raw("SELECT * FROM ruff_events WHERE  DATE(start_date) = DATE 'tomorrow' ORDER BY {0} DESC".format(type))
            else:
                event_lists = cls.objects.raw("SELECT * FROM ruff_events WHERE  DATE(start_date) = DATE 'tomorrow' ORDER BY {0} ASC".format(type))
        elif(date == "week"):
            if(type == "start_date" or type == "cost"):
                event_lists = cls.objects.raw("SELECT * FROM ruffguide.ruff_events WHERE  DATE(start_date) BETWEEN date_trunc('week', current_date::timestamp)::date AND (date_trunc('week', current_date::timestamp)+ '6 days'::interval)::date ORDER BY {0} DESC".format(type))
            else:
                event_lists = cls.objects.raw("SELECT * FROM ruffguide.ruff_events WHERE  DATE(start_date) BETWEEN date_trunc('week', current_date::timestamp)::date AND (date_trunc('week', current_date::timestamp)+ '6 days'::interval)::date ORDER BY {0} ASC".format(type))
        else:
            if (type == "start_date" or type == "cost"):
                event_lists = cls.objects.all().order_by('-' + type)
            else:
                event_lists = cls.objects.all().order_by(type)
        return event_lists

    @classmethod
    def type_filter(cls, types, selected):
        event_lists = cls.objects.filter(type__in=types).order_by('-' + selected)
        return event_lists


    @classmethod
    def get_single(cls, event_id):
        single_event = cls.objects.filter(id=event_id)
        return single_event[0] if len(single_event) > 0 else {}

class Restaurants(models.Model):
    type = models.CharField(max_length=50, default="", null=False)
    name = models.TextField(default="", null=False)
    building_name = models.CharField(max_length=50, default="")
    street_number = models.IntegerField(default=0)
    street_name = models.CharField(max_length=20, default="", null=False)
    suburb = models.CharField(max_length=50, default="", null=False)
    postcode = models.CharField(max_length=10, default="", null=False)
    state = models.CharField(max_length=50, default="")
    phone_number = models.CharField(max_length=20,default="")
    description = models.TextField(default="", null=False)
    date = models.DateTimeField(auto_now_add=True)
    days_of_week = models.TextField(default="", null=False)
    photo = ArrayField(models.URLField(default=""), default=[])

    @classmethod
    def create(cls, restaurant_detail):
        restaurant_save = cls(type=restaurant_detail["type"], description=restaurant_detail["description"],
                         name=restaurant_detail["name"], building_name=restaurant_detail["building_name"],
                         street_number=restaurant_detail["street_number"], street_name=restaurant_detail["street_name"],
                         suburb=restaurant_detail["suburb"], postcode=restaurant_detail["postcode"],
                         state=restaurant_detail["state"], phone_number=restaurant_detail["phone_number"], days_of_week=restaurant_detail["days_of_week"], photo=restaurant_detail["photo"])
        restaurant_save.save()
        return restaurant_save

    @classmethod
    def update(cls, restaurant_detail):
        single_restaurant = cls.objects.filter(id=restaurant_detail["id"]).get()
        single_restaurant.type = restaurant_detail["type"]
        single_restaurant.description = restaurant_detail["description"]
        single_restaurant.name = restaurant_detail["name"]
        single_restaurant.building_name = restaurant_detail["building_name"]
        single_restaurant.street_number = restaurant_detail["street_number"]
        single_restaurant.street_name = restaurant_detail["street_name"]
        single_restaurant.suburb = restaurant_detail["suburb"]
        single_restaurant.postcode = restaurant_detail["postcode"]
        single_restaurant.state = restaurant_detail["state"]
        single_restaurant.phone_number = restaurant_detail["phone_number"]
        single_restaurant.days_of_week = restaurant_detail["days_of_week"]
        single_restaurant.photo = restaurant_detail["photo"]
        single_restaurant.save()
        return single_restaurant

    @classmethod
    def delete_single(cls, restaurant_id):
        status = cls.objects.filter(id=restaurant_id).delete()
        return {"status": status}

    @classmethod
    def get(cls, type):
        restaurant_lists = cls.objects.all().order_by('-' + type)
        return restaurant_lists

    @classmethod
    def get_single(cls, restaurant_id):
        restaurants = cls.objects.filter(id=restaurant_id)
        single_restaurant = restaurants[0] if len(restaurants) > 0 else {}
        restaurant_reviews_lists = RestaurantReviews.objects.filter(restaurant_id=restaurant_id).order_by('-date')
        restaurant_detail = {"single_restaurant": single_restaurant, "review": restaurant_reviews_lists}
        return restaurant_detail

class RestaurantReviews(models.Model):
    restaurant = models.ForeignKey(Restaurants, on_delete=models.CASCADE)
    user = models.CharField(max_length=200, null=False)
    date = models.DateTimeField(auto_now_add=True, null=False)
    comments = models.TextField(default="", null=False)
    rating = models.FloatField(default=0.0, null=False)
    photo = ArrayField(models.URLField(default=""), default=[])

    @classmethod
    def create(cls, review_detail, restaurant_id):
        restaurant_review_save = cls(restaurant_id=restaurant_id, user=review_detail["user"],
                               comments=review_detail["comments"], rating=review_detail["rating"], photo=review_detail["photo"])
        restaurant_review_save.save()
        return restaurant_review_save


    @classmethod
    def delete_single(cls, review_id):
        status = cls.objects.filter(id=review_id).delete()
        return {"status": status}

class Hikes(models.Model):
    location = models.TextField(default="", null=False)
    name = models.TextField(default="", null=False)
    distance = models.FloatField(default=0.0, null=False)
    leash = models.BooleanField(default=False)
    coordinates = models.TextField(default="", null=False)
    difficulty = models.CharField(max_length=100, default="")
    description = models.TextField(default="", null=False)
    date = models.DateTimeField(auto_now_add=True)
    more_info_link = models.URLField(default="", max_length=300)
    days_of_week = models.TextField(default="", null=False)
    photo = ArrayField(models.URLField(default=""), default=[])

    @classmethod
    def create(cls, hike_detail):
        hike_save = cls(location=hike_detail["location"], name=hike_detail["name"],
                        distance=hike_detail["distance"], coordinates=hike_detail["coordinates"],
                        difficulty=hike_detail["difficulty"], description=hike_detail["description"],
                        more_info_link=hike_detail["location"], days_of_week=hike_detail["days_of_week"],
                        leash=hike_detail["leash"], photo=hike_detail["photo"])
        hike_save.save()
        return hike_save

    @classmethod
    def update(cls, hike_detail):
        single_hike = cls.objects.filter(id=hike_detail["id"]).get()
        single_hike.location = hike_detail["location"]
        single_hike.name = hike_detail["name"]
        single_hike.distance = hike_detail["distance"]
        single_hike.coordinates = hike_detail["coordinates"]
        single_hike.difficulty = hike_detail["difficulty"]
        single_hike.description = hike_detail["description"]
        single_hike.more_info_link = hike_detail["location"]
        single_hike.days_of_week = hike_detail["days_of_week"]
        single_hike.leash = hike_detail["leash"]
        single_hike.photo = hike_detail["photo"]

        single_hike.save()
        return single_hike

    @classmethod
    def delete_single(cls, hike_id):
        status = cls.objects.filter(id=hike_id).delete()
        return {"status": status}

    @classmethod
    def get(cls, type):
        hikes_lists = cls.objects.all().order_by('-' + type)
        return hikes_lists

    @classmethod
    def difficulty_filter(cls, type):
        hikes_lists = cls.objects.filter(difficulty=type).order_by('-name')
        return hikes_lists

    @classmethod
    def leash_filter(cls, option):
        hikes_lists = cls.objects.filter(leash=option).order_by('-name')
        return hikes_lists

    @classmethod
    def get_single(cls, hike_id):
        hikes = cls.objects.filter(id=hike_id)
        single_hike = hikes[0] if len(hikes) > 0 else {}
        hike_reviews_lists = HikeReviews.objects.filter(hike_id=hike_id).order_by('-date')
        hike_detail = {"single_hike": single_hike, "review": hike_reviews_lists}
        return hike_detail

    @classmethod
    def filter(cls, leash, type, difficulty):
        if(difficulty == "all"):
            hikes_lists = cls.objects.filter(leash=leash).order_by('-' + type)
        else:
            hikes_lists = cls.objects.filter(leash=leash, difficulty=difficulty).order_by('-' + type)
        return hikes_lists

class HikeReviews(models.Model):
    hike = models.ForeignKey(Hikes, on_delete=models.CASCADE)
    user = models.CharField(max_length=200, null=False)
    comments = models.TextField(default="", null=False)
    date = models.DateTimeField(auto_now_add=True, null=False)
    rating = models.FloatField(default=0.0, null=False)
    photo = ArrayField(models.URLField(default=""), default=[])

    @classmethod
    def create(cls, review_detail, hike_id):
        hike_review_save = cls(hike_id=hike_id, user=review_detail["user"], comments=review_detail["comments"], rating=review_detail["rating"], photo=review_detail["photo"])
        hike_review_save.save()
        return hike_review_save

    @classmethod
    def delete_single(cls, review_id):
        status = cls.objects.filter(id=review_id).delete()
        return {"status": status}

class Parks(models.Model):
    location = models.TextField(default="", null=False)
    name = models.TextField(default="", null=False)
    days_of_week = models.TextField(default="", null=False)
    description = models.TextField(default="", null=False)
    photo = ArrayField(models.URLField(default=""), default=[])
    leash = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)


    @classmethod
    def create(cls, park_detail):
        park_save = cls(location=park_detail["location"], description=park_detail["description"],
                        days_of_week=park_detail["days_of_week"], name=park_detail["name"],
                        photo=park_detail["photo"], leash=park_detail["leash"])
        park_save.save()
        return park_save

    @classmethod
    def update(cls, park_detail):
        single_park = cls.objects.filter(id=park_detail["id"]).get()
        single_park.location = park_detail["location"]
        single_park.description = park_detail["description"]
        single_park.days_of_week = park_detail["days_of_week"]
        single_park.name = park_detail["name"]
        single_park.photo = park_detail["photo"]
        single_park.leash = park_detail["leash"]
        single_park.save()
        return single_park

    @classmethod
    def delete_single(cls, park_id):
        status = cls.objects.filter(id=park_id).delete()
        return {"status": status}

    @classmethod
    def get(cls, type):
        parks_lists = cls.objects.all().order_by('-' + type)
        return parks_lists

    @classmethod
    def leash_filter(cls, option):
        parks_lists = cls.objects.filter(leash=option).order_by('-name')
        return parks_lists

    @classmethod
    def filter(cls, leash, type):
        parks_lists = cls.objects.filter(leash=leash).order_by('-' + type)
        return parks_lists

    @classmethod
    def get_single(cls, park_id):
        parks = cls.objects.filter(id=park_id)
        single_park = parks[0] if len(parks) > 0 else {}
        park_reviews_lists = ParkReviews.objects.filter(park_id=park_id).order_by('-date')
        park_detail = {"single_park": single_park, "review": park_reviews_lists}
        return park_detail


class ParkReviews(models.Model):
    park = models.ForeignKey(Parks, on_delete=models.CASCADE)
    user = models.CharField(max_length=200, null=False)
    date = models.DateTimeField(auto_now_add=True, null=False)
    comments = models.TextField(default="", null=False)
    rating = models.FloatField(default=0.0, null=False)
    photo = ArrayField(models.URLField(default=""), default=[])

    @classmethod
    def create(cls, review_detail, park_id):
        park_review_save = cls(park_id=park_id, user=review_detail["user"], comments=review_detail["comments"], rating=review_detail["rating"], photo=review_detail["photo"])
        park_review_save.save()
        return park_review_save


    @classmethod
    def delete_single(cls, review_id):
        status = cls.objects.filter(id=review_id).delete()
        return {"status": status}