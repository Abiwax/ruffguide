from django.contrib import admin

from .models import *
# Register your models here.
admin.site.register(Events)
admin.site.register(Restaurants)
admin.site.register(RestaurantReviews)
admin.site.register(Hikes)
admin.site.register(HikeReviews)
admin.site.register(Parks)
admin.site.register(ParkReviews)