$(document).ready(function () {
    $('#example').DataTable();
    $('.input-daterange').datepicker({});
    $('#start_time').datetimepicker({format: 'LT'});
    $('#end_time').datetimepicker({format: 'LT'});
    $('#datetimepicker6').datetimepicker({format: 'LT'});

    $('#datetimepicker7').datetimepicker({
        format: 'LT',
        useCurrent: false //Important! See issue #1075
    });
    $("#datetimepicker6").on("dp.change", function (e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepicker7").on("dp.change", function (e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
});