function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
    ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

function setNavigator(current_location) {
    var user_location = localStorage.getItem('UL');
    var json_location = JSON.parse(user_location);
    var csrf_token = $.cookie('csrftoken');

    if (navigator) {
        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition(function (position) {
                var string_location = JSON.stringify({
                    "lat": position.coords.latitude,
                    "lng": position.coords.longitude
                });
                var map_shown = ($("#mapModal").data('bs.modal') || {})._isShown;
                var value_map_shown = map_shown ? map_shown === true : false;

                if (user_location === undefined || user_location === null) {
                    localStorage.setItem('UL', string_location);
                    $('#loadingModal').modal('toggle');
                    callAjax(value_map_shown, csrf_token, position.coords.latitude, position.coords.longitude);
                }
                else if (getDistanceFromLatLonInKm(json_location.lat, json_location.lng, position.coords.latitude, position.coords.longitude) > 0.1) {
                    localStorage.setItem('UL', string_location);
                    $('#loadingModal').modal('toggle');
                    callAjax(value_map_shown, csrf_token, position.coords.latitude, position.coords.longitude);
                }
                else {

                    $.ajax({
                        type: "POST",
                        url: "/location_stored/",
                        headers: {"X-CSRFToken": csrf_token}
                    }).done(function (response) {
                        if (response.location === false) {
                            $('#loadingModal').modal('toggle');
                            callAjax(value_map_shown, csrf_token, user_location.lat, user_location.lng);
                        }
                        // else {
                        //     callAddress(false, json_location.lat, json_location.lng)
                        // }
                    });
                }
            });
        }
    }
}

function callAjax(map_shown, csrf_token, latitude, longitude) {
    if (map_shown) {
        $('#mapModal').modal('toggle');
    }
    $.ajax({
        url: "/park/location/",
        type: 'POST',
        headers: {"X-CSRFToken": csrf_token},
        data: {latitude: latitude, longitude: longitude},
        success: function (data) {
            var returned_html = $(data);
            $("#view").empty().append(returned_html);
            $('html,body').animate({
                scrollTop: $("body").offset().top
            });
            $('#loadingModal').modal('toggle');
            if (map_shown) {
                $('#mapModal').modal('toggle');
                mapInitialization();
            }
        },
        error: function (err) {
            if (err.statusCode === 403) {
                $("#error_message_jax").html(err.responseText).css('display', 'block');
            } else {
                $("#error_message_jax").html(err.responseText).css('display', 'block');
            }
            $('#loadingModal').modal('toggle');
            if (map_shown) {
                $('#mapModal').modal('toggle');
                mapInitialization();
            }
        }
    });
}

function callFilter() {
    var address = $('#location').val();
    var slider = document.getElementById("range_distance");
    var leash = $('#check_input').is(':checked');
    var selected = $('#select_sort').val();
    filterView(address, slider.value, leash, selected);
}

function callAddress(current_location, latitude, longitude) {
    $.ajax({
        type: "POST",
        url: "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDemTKrKvU6qAmvZIvC-J4JU5na3C-mGV4&latlng=" + latitude + "," + longitude + "&sensor=true"
    }).done(function (response) {
        result = response.results[0];
        var address = result.formatted_address;
        $("#location").val(address);
        if(current_location){
            callFilter()
        }
    });
}

$(document).ready(function () {
    setNavigator();
});