# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-04 02:41
from __future__ import unicode_literals

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ruff', '0007_auto_20171104_0127'),
    ]

    operations = [
        migrations.AddField(
            model_name='events',
            name='photo',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.TextField(default=''), default=[], size=None),
        ),
        migrations.AddField(
            model_name='hikereviews',
            name='photo',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.TextField(default=''), default=[], size=None),
        ),
        migrations.AddField(
            model_name='hikes',
            name='photo',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.TextField(default=''), default=[], size=None),
        ),
        migrations.AddField(
            model_name='parkreviews',
            name='photo',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.TextField(default=''), default=[], size=None),
        ),
        migrations.AddField(
            model_name='parks',
            name='photo',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.TextField(default=''), default=[], size=None),
        ),
        migrations.AddField(
            model_name='restaurantreviews',
            name='photo',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.TextField(default=''), default=[], size=None),
        ),
        migrations.AddField(
            model_name='restaurants',
            name='photo',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.TextField(default=''), default=[], size=None),
        ),
    ]
