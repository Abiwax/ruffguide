# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-02 02:40
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ruff', '0002_events_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hikes',
            name='leash',
        ),
        migrations.RemoveField(
            model_name='parks',
            name='leash',
        ),
    ]
