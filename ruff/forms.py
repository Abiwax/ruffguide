from django import forms


class EventsForm(forms.Form):
    type = forms.CharField(max_length=50)
    description = forms.TextInput()
    organiser = forms.CharField(max_length=100)
    cost = forms.TextInput()
    contact = forms.CharField(max_length=200)
    website = forms.URLField()
    location = forms.CharField(max_length=200)
    start_date = forms.CharField(max_length=15)
    end_date = forms.CharField(max_length=15)
    start_time = forms.CharField(max_length=10)
    end_time = forms.CharField(max_length=10)

class HikesForm(forms.Form):
    name = forms.CharField(max_length=50)
    location = forms.TextInput()
    distance = forms.FloatField()
    description = forms.TextInput()
    coordinates = forms.TextInput()
    difficulty = forms.CharField(max_length=200)
    more_info_link = forms.CharField(max_length=100, required=False)
    days_of_week = forms.TextInput()

class RestaurantsForm(forms.Form):
    name = forms.CharField(max_length=50)
    type = forms.TextInput()
    building_name = forms.TextInput()
    street_number = forms.IntegerField()
    street_name = forms.CharField(max_length=50)
    suburb = forms.CharField(max_length=50)
    postcode = forms.CharField(max_length=50)
    state = forms.CharField(max_length=50)
    phone_number = forms.CharField(max_length=50)
    description = forms.TextInput()
    days_of_week = forms.TextInput()

class ParksForm(forms.Form):
    name = forms.CharField(max_length=50)
    location = forms.TextInput()
    description = forms.TextInput()
    days_of_week = forms.TextInput()

class Restaurant_Review_Form(forms.Form):
    user = forms.CharField(max_length=50)
    rating = forms.TextInput()
    comments = forms.TextInput()

class Park_Review_Form(forms.Form):
    user = forms.CharField(max_length=50)
    rating = forms.TextInput()
    comments = forms.TextInput()

class Hike_Review_Form(forms.Form):
    user = forms.CharField(max_length=50)
    rating = forms.TextInput()
    comments = forms.TextInput()